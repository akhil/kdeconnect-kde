# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the kdeconnect-kde package.
# Vit Pelcak <vit@pelcak.org>, 2020, 2021.
#
msgid ""
msgstr ""
"Project-Id-Version: kdeconnect-kde\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-09-12 00:45+0000\n"
"PO-Revision-Date: 2021-08-10 14:32+0200\n"
"Last-Translator: Vit Pelcak <vpelcak@suse.cz>\n"
"Language-Team: Czech <kde-i18n-doc@kde.org>\n"
"Language: cs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"
"X-Generator: Lokalize 21.04.3\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Vít Pelčák"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "vit@pelcak.org"

#: main.cpp:30 main.cpp:32
#, kde-format
msgid "KDE Connect"
msgstr "KDE Connect"

#: main.cpp:34
#, kde-format
msgid "(c) 2015, Aleix Pol Gonzalez"
msgstr "(C) 2015 Aleix Pol Gonzalez"

#: main.cpp:35
#, kde-format
msgid "Aleix Pol Gonzalez"
msgstr "Aleix Pol Gonzalez"

#: main.cpp:35
#, kde-format
msgid "Maintainer"
msgstr "Správce"

#: main.cpp:51
#, kde-format
msgid "URL to share"
msgstr "URL ke sdílení"

#: qml/DevicePage.qml:24
#, kde-format
msgid "Unpair"
msgstr "Zrušit párování"

#: qml/DevicePage.qml:29
#, kde-format
msgid "Send Ping"
msgstr "Poslat ping"

#: qml/DevicePage.qml:37 qml/PluginSettings.qml:16
#, kde-format
msgid "Plugin Settings"
msgstr "Nastavení modulů"

#: qml/DevicePage.qml:63
#, kde-format
msgid "Multimedia control"
msgstr "Ovládání multimédií"

#: qml/DevicePage.qml:70
#, kde-format
msgid "Remote input"
msgstr "Vzdálený vstup"

#: qml/DevicePage.qml:77 qml/presentationRemote.qml:15
#, kde-format
msgid "Presentation Remote"
msgstr "Ovladač prezentace"

#: qml/DevicePage.qml:86 qml/mousepad.qml:44
#, kde-format
msgid "Lock"
msgstr "Uzamknout"

#: qml/DevicePage.qml:86
#, kde-format
msgid "Unlock"
msgstr "Odemknout"

#: qml/DevicePage.qml:93
#, kde-format
msgid "Find Device"
msgstr "Najít zařízení"

#: qml/DevicePage.qml:98 qml/runcommand.qml:16
#, kde-format
msgid "Run command"
msgstr "Spustit příkaz"

#: qml/DevicePage.qml:106
#, kde-format
msgid "Share File"
msgstr "Sdílet soubor"

#: qml/DevicePage.qml:111 qml/volume.qml:16
#, kde-format
msgid "Volume control"
msgstr "Ovládání hlasitosti"

#: qml/DevicePage.qml:120
#, kde-format
msgid "This device is not paired"
msgstr "Toto zařízení není spárováno"

#: qml/DevicePage.qml:124 qml/FindDevicesPage.qml:23
#, kde-format
msgid "Pair"
msgstr "Spárovat"

#: qml/DevicePage.qml:136
#, kde-format
msgid "Pair requested"
msgstr "Bylo vyžádáno párování"

#: qml/DevicePage.qml:142
#, kde-format
msgid "Accept"
msgstr "Přijmout"

#: qml/DevicePage.qml:148
#, kde-format
msgid "Reject"
msgstr "Odmítnout"

#: qml/DevicePage.qml:157
#, kde-format
msgid "This device is not reachable"
msgstr "Toto zařízení je nedostupné"

#: qml/DevicePage.qml:165
#, kde-format
msgid "Please choose a file"
msgstr "Prosím, vyberte soubor"

#: qml/FindDevicesPage.qml:38
#, kde-format
msgid "No devices found"
msgstr "Nebyla nalezena žádná zařízení"

#: qml/FindDevicesPage.qml:51
#, kde-format
msgid "Remembered"
msgstr "Zapamatovaná"

#: qml/FindDevicesPage.qml:53
#, kde-format
msgid "Available"
msgstr "Dostupná"

#: qml/FindDevicesPage.qml:55
#, kde-format
msgid "Connected"
msgstr "Připojená"

#: qml/main.qml:28
#, kde-format
msgid "Find devices..."
msgstr "Najít zařízení..."

#: qml/mousepad.qml:16
#, kde-format
msgid "Remote Control"
msgstr "Dálkové ovládání"

#: qml/mousepad.qml:59
#, kde-format
msgid "Press the left and right mouse buttons at the same time to unlock"
msgstr "Pro odemčení stiskněte levé a pravé tlačítko myši najednou"

#: qml/mpris.qml:19
#, kde-format
msgid "Multimedia Controls"
msgstr "Ovládání multimédií"

#: qml/mpris.qml:61
#, kde-format
msgid "No players available"
msgstr "Nejsou dostupné žádné přehrávače"

#: qml/mpris.qml:109
#, kde-format
msgid "%1 - %2"
msgstr "%1 - %2"

#: qml/presentationRemote.qml:20
#, kde-format
msgid "Enable Full-Screen"
msgstr "Povolit celou obrazovku"

#: qml/runcommand.qml:21
#, kde-format
msgid "Edit commands"
msgstr "Upravit příkazy"

#: qml/runcommand.qml:24
#, kde-format
msgid "You can edit commands on the connected device"
msgstr "Můžete upravit příkazy na připojeném zařízení"

#: qml/runcommand.qml:43
#, kde-format
msgid "No commands defined"
msgstr "Nebyly zadány žádné příkazy"
