# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the kdeconnect-kde package.
#
# Łukasz Wojniłowicz <lukasz.wojnilowicz@gmail.com>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: kdeconnect-kde\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-09-12 00:45+0000\n"
"PO-Revision-Date: 2021-08-15 08:09+0200\n"
"Last-Translator: Łukasz Wojniłowicz <lukasz.wojnilowicz@gmail.com>\n"
"Language-Team: Polish <kde-i18n-doc@kde.org>\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2);\n"
"X-Generator: Lokalize 20.12.1\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Łukasz Wojniłowicz"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "lukasz.wojnilowicz@gmail.com"

#: main.cpp:30 main.cpp:32
#, kde-format
msgid "KDE Connect"
msgstr "KDE Connect"

#: main.cpp:34
#, kde-format
msgid "(c) 2015, Aleix Pol Gonzalez"
msgstr "(c) 2015, Aleix Pol Gonzalez"

#: main.cpp:35
#, kde-format
msgid "Aleix Pol Gonzalez"
msgstr "Aleix Pol Gonzalez"

#: main.cpp:35
#, kde-format
msgid "Maintainer"
msgstr "Opiekun"

#: main.cpp:51
#, kde-format
msgid "URL to share"
msgstr "Adres URL do udostępnienia"

#: qml/DevicePage.qml:24
#, kde-format
msgid "Unpair"
msgstr "Odparuj"

#: qml/DevicePage.qml:29
#, kde-format
msgid "Send Ping"
msgstr "Wyślij ping"

#: qml/DevicePage.qml:37 qml/PluginSettings.qml:16
#, kde-format
msgid "Plugin Settings"
msgstr "Ustawienia wtyczki"

#: qml/DevicePage.qml:63
#, kde-format
msgid "Multimedia control"
msgstr "Sterowanie odtwarzaczami"

#: qml/DevicePage.qml:70
#, kde-format
msgid "Remote input"
msgstr "Zdalna obsługa"

#: qml/DevicePage.qml:77 qml/presentationRemote.qml:15
#, kde-format
msgid "Presentation Remote"
msgstr "Obsługa prezentacji"

#: qml/DevicePage.qml:86 qml/mousepad.qml:44
#, kde-format
msgid "Lock"
msgstr "Zablokuj"

#: qml/DevicePage.qml:86
#, kde-format
msgid "Unlock"
msgstr "Odblokuj"

#: qml/DevicePage.qml:93
#, kde-format
msgid "Find Device"
msgstr "Znajdź urządzenie"

#: qml/DevicePage.qml:98 qml/runcommand.qml:16
#, kde-format
msgid "Run command"
msgstr "Wykonaj polecenie"

#: qml/DevicePage.qml:106
#, kde-format
msgid "Share File"
msgstr "Udostępnij plik"

#: qml/DevicePage.qml:111 qml/volume.qml:16
#, kde-format
msgid "Volume control"
msgstr "Sterowanie głośnością"

#: qml/DevicePage.qml:120
#, kde-format
msgid "This device is not paired"
msgstr "To urządzenie nie jest sparowane"

#: qml/DevicePage.qml:124 qml/FindDevicesPage.qml:23
#, kde-format
msgid "Pair"
msgstr "Sparuj"

#: qml/DevicePage.qml:136
#, kde-format
msgid "Pair requested"
msgstr "Zażądano parowania"

#: qml/DevicePage.qml:142
#, kde-format
msgid "Accept"
msgstr "Przyjmij"

#: qml/DevicePage.qml:148
#, kde-format
msgid "Reject"
msgstr "Odrzuć"

#: qml/DevicePage.qml:157
#, kde-format
msgid "This device is not reachable"
msgstr "To urządzenie jest nieosiągalne"

#: qml/DevicePage.qml:165
#, kde-format
msgid "Please choose a file"
msgstr "Wybierz plik"

#: qml/FindDevicesPage.qml:38
#, kde-format
msgid "No devices found"
msgstr "Nie znaleziono żadnych urządzeń"

#: qml/FindDevicesPage.qml:51
#, kde-format
msgid "Remembered"
msgstr "Zapamiętane"

#: qml/FindDevicesPage.qml:53
#, kde-format
msgid "Available"
msgstr "Dostępne"

#: qml/FindDevicesPage.qml:55
#, kde-format
msgid "Connected"
msgstr "Podłączone"

#: qml/main.qml:28
#, kde-format
msgid "Find devices..."
msgstr "Znajdź urządzenia..."

#: qml/mousepad.qml:16
#, kde-format
msgid "Remote Control"
msgstr "Zdalne sterowanie"

#: qml/mousepad.qml:59
#, kde-format
msgid "Press the left and right mouse buttons at the same time to unlock"
msgstr ""
"Naciśnij lewy i prawy przycisk myszy w tym samym czasie, aby odblokować"

#: qml/mpris.qml:19
#, kde-format
msgid "Multimedia Controls"
msgstr "Sterowanie odtwarzaczem"

#: qml/mpris.qml:61
#, kde-format
msgid "No players available"
msgstr "Nie znaleziono żadnego odtwarzacza"

#: qml/mpris.qml:109
#, kde-format
msgid "%1 - %2"
msgstr "%1 - %2"

#: qml/presentationRemote.qml:20
#, kde-format
msgid "Enable Full-Screen"
msgstr "Włącz pełny ekran"

#: qml/runcommand.qml:21
#, kde-format
msgid "Edit commands"
msgstr "Zmień polecenia"

#: qml/runcommand.qml:24
#, kde-format
msgid "You can edit commands on the connected device"
msgstr "Możesz zmienić polecenia na podłączonym urządzeniu"

#: qml/runcommand.qml:43
#, kde-format
msgid "No commands defined"
msgstr "Nie określono poleceń"
